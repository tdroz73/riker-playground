use riker::actors::*;

#[derive(Clone, Debug)]
pub struct PowerStatus {
    pub percent_remaining: u32,
}

#[actor(PowerStatus)]
pub struct GpsActor {
    chan: ChannelRef<PowerStatus>,
}

#[actor(PowerStatus)]
pub struct NavigationActor {
    chan: ChannelRef<PowerStatus>,
}

impl Actor for GpsActor {
    type Msg = GpsActorMsg;

    fn pre_start(&mut self, ctx: &Context<Self::Msg>) {
        let sub = Box::new(ctx.myself());
        self.chan.tell(
            Subscribe {
                actor: sub,
                topic: "*".into(),
            },
            None,
        );
    }

    fn recv(&mut self, ctx: &Context<Self::Msg>, msg: Self::Msg, sender: Sender) {
        self.receive(ctx, msg, sender);
    }
}

impl ActorFactoryArgs<ChannelRef<PowerStatus>> for GpsActor {
    fn create_args(args: ChannelRef<PowerStatus>) -> Self {
        GpsActor { chan: args }
    }
}

impl Receive<PowerStatus> for GpsActor {
    type Msg = GpsActorMsg;

    fn receive(&mut self, _ctx: &Context<Self::Msg>, msg: PowerStatus, _sender: Sender) {
        println!(
            "GpsActor - PowerStatus received, percent remaining: {}",
            msg.percent_remaining
        );
        if msg.percent_remaining == 10 {
            println!("!!! WARNING (GPS) - Limited power left - GPS accuracy being reduced to save power.")
        }
    }
}

impl Actor for NavigationActor {
    type Msg = NavigationActorMsg;

    fn pre_start(&mut self, ctx: &Context<Self::Msg>) {
        let sub = Box::new(ctx.myself());
        self.chan.tell(
            Subscribe {
                actor: sub,
                topic: "*".into(),
            },
            None,
        );
    }

    fn recv(&mut self, ctx: &Context<Self::Msg>, msg: Self::Msg, sender: Sender) {
        self.receive(ctx, msg, sender);
    }
}

impl ActorFactoryArgs<ChannelRef<PowerStatus>> for NavigationActor {
    fn create_args(args: ChannelRef<PowerStatus>) -> Self {
        NavigationActor { chan: args }
    }
}

impl Receive<PowerStatus> for NavigationActor {
    type Msg = NavigationActorMsg;

    fn receive(&mut self, _ctx: &Context<Self::Msg>, msg: PowerStatus, _sender: Sender) {
        println!(
            "NavigationActor - PowerStatus received, percent remaining: {}",
            msg.percent_remaining
        );
        if msg.percent_remaining == 20 {
            println!(
                "!!! WARNING (Navigation) - Reduced power mode - some functions are being disabled to save charge!"
            )
        }
    }
}
