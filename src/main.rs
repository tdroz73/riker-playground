use std::time::Duration;

use riker::actors::*;

use counter::*;

use crate::channels::{GpsActor, NavigationActor, PowerStatus};

mod channels;
mod counter;

fn main() {
    println!("Setting up actor system...");
    let sys = ActorSystem::new().unwrap();
    println!("Getting channel ref...");
    let chan: ChannelRef<PowerStatus> = channel("power-status", &sys).unwrap();
    println!("Setting up actors...");
    let props = Props::new::<Counter>();
    let actor = sys.actor_of_props("counter", props).unwrap();
    println!("Counter setup!");
    let _gps_actor = sys
        .actor_of_args::<GpsActor, _>("gps", chan.clone())
        .unwrap();
    println!("GPS setup!");
    let _nav_actor = sys
        .actor_of_args::<NavigationActor, _>("nav", chan.clone())
        .unwrap();
    println!("Navigation setup!");
    println!("Done - telling actors what to do...");
    actor.tell(Add, None);
    actor.tell(Add, None);
    actor.tell(Sub, None);
    actor.tell(Print, None);

    let mut count = 0u32;

    println!("Let's send PowerStatus events...");

    loop {
        let remaining = 100 - count;
        let stat = PowerStatus {
            percent_remaining: remaining,
        };
        chan.tell(
            Publish {
                msg: stat,
                topic: "power".into(),
            },
            None,
        );
        count += 1;
        if count == 100 {
            println!("Ok...out of power...");
            break;
        }
    }
    println!("Ok, we're done!");
    std::thread::sleep(Duration::from_millis(500));
}
